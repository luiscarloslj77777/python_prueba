print("Calcular el area de un circulo")
radioI = float(input("Ingrese el radio del circulo en milimetros: "))
print(f'El radio del circulo es: {radioI}')
areaC = 3.1416 * (radioI*radioI)
dosDecimales = "{0:.2f}".format(areaC)
print(f'El area del circulo es: {dosDecimales} mm²')